# devara.world overview

## Getting started

well how to start?


## Repositories

| Type | Repository |  Description | Pipeline | Schedule Pipeline |
| --- | --- | --- | --- | --- |
| Documentation | [overview](https://gitlab.com/devara.world/overview) | main documentation. gives an overview about devara.world | - | - |
| Deployment | [terraform-deploy](https://gitlab.com/devara.world/terraform-deploy/) | the terraform deployment for the whole devara.world | ![terraform-deploy-status](https://gitlab.com/devara.world/terraform-deploy/badges/main/pipeline.svg) | 1x/month |
| Instance-Image | [docker-server-packer](https://gitlab.com/devara.world/docker-server-packer/) | build the instance image for a docker host using packer | ![docker-server-packer-status](https://gitlab.com/devara.world/docker-server-packer/badges/main/pipeline.svg) | - |
| Instance-Image | [admin-server-packer](https://gitlab.com/devara.world/admin-server-packer/) | build the instance image for a admin host using packer | ![admin-server-packer-status](https://gitlab.com/devara.world/admin-server-packer/badges/main/pipeline.svg) | - |
| Instance-Image | [base-server-stage2-packer](https://gitlab.com/devara.world/base-server-stage2-packer/) | build the base instance image in stage 2 | ![base-server-stage2-packer-status](https://gitlab.com/devara.world/base-server-stage2-packer/badges/main/pipeline.svg) | - |
| Instance-Image | [base-server-stage1-packer](https://gitlab.com/devara.world/base-server-stage1-packer/) | build the base instance image in stage 1 | ![base-server-stage1-packer-status](https://gitlab.com/devara.world/base-server-stage1-packer/badges/main/pipeline.svg) | - |
| App-Stack | [mqtt-sensor-stack](https://gitlab.com/devara.world/mqtt-sensor-stack/) | the MQTT serevr stack with mosquitto, telegraf, influxdb and grafana | ![mqtt-sensor-stack-status](https://gitlab.com/devara.world/mqtt-sensor-stack/badges/main/pipeline.svg) | - |
| App | [health-app](https://gitlab.com/devara.world/health-app/) | a health app | ![health-app-status](https://gitlab.com/devara.world/health-app/badges/main/pipeline.svg) | 1x/week |
| Build-Image | [cargo-chef-image](https://gitlab.com/devara.world/cargo-chef-image/) | build the cargo chef-chef build-image for rust projects | ![cargo-chef-image-status](https://gitlab.com/devara.world/cargo-chef-image/badges/main/pipeline.svg) | 1x/month |
| Build-Image | [terraform-image](https://gitlab.com/devara.world/terraform-image/) | build the terraform execution image for CICD pipelines | ![terraform-image-status](https://gitlab.com/devara.world/terraform-image/badges/main/pipeline.svg) | - |
| Build-Image | [packer-image](https://gitlab.com/devara.world/packer-image/) | build the packer execution image for CICD pipelines | ![packer-image-status](https://gitlab.com/devara.world/packer-image/badges/main/pipeline.svg) | - |
| Build-Image | [build-base-image](https://gitlab.com/devara.world/build-base-image/) | build the base execution image for CICD pipelines | ![build-base-image-status](https://gitlab.com/devara.world/build-base-image/badges/main/pipeline.svg) | 1x/week |


### Flowshart


```plantuml
partition "Build Images" {
"build-base-image scheduler 1x/month" --> "build-base-image"
"build-base-image" --> "terraform-image"
"build-base-image" --> "packer-image"
"cargo-chef-image scheduler 1x/month" --> "cargo-chef-image"
}

partition "Instance Images" {
"packer-image" --> "base-server-stage1-packer"
"base-server-stage1-packer" --> "base-server-stage2-packer"
"base-server-stage2-packer" --> "docker-server-packer"
"base-server-stage2-packer" --> "admin-server-packer"
}


partition "Deployment" {
"terraform-deploy scheduler 1x/month" --> "terraform-deploy"
"terraform-image" --> "terraform-deploy"
"admin-server-packer" --> "terraform-deploy"
"docker-server-packer" --> "terraform-deploy"
"terraform-deploy" --> (*)
}

partition "App" {
"Scheduler 1x/week" --> "health-app"
"cargo-chef-image" --> "health-app"
"health-app" --> "admin-server-packer"
"health-app" --> "docker-server-packer"
}
```


### Access

* admin-server-packer has access to health-app


## Registries

* Terraform Registries
  * https://gitlab.com/devara.world/terraform-deploy/
* Container Registries
  * https://gitlab.com/devara.world/health-app/

## Server Infrastructure

* hosting on Hetzner
* DNS via Namecheap 

## Secrets

* Access to other git repositories via [Gitlab CI Job Token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)
* Access to terraform registry via [Gitlab Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens)

## Further Readings

* [Gitlab Token Overview](https://github.com/git-guides/git-pull)
* [Gitlab Scheduled Pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
* [Gitlab Cron](https://docs.gitlab.com/ee/topics/cron/index.html)
* [Terraform Integration in Merge Requests](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)
* [Bagges](https://docs.gitlab.com/ee/user/project/badges.html)
* [Handbook Markdown Guide](https://about.gitlab.com/handbook/markdown-guide/)
* [PlantUML Activity Diagram](https://plantuml.com/activity-diagram-legacy)
* [Gitlab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* https://github.com/git-guides/git-pull

